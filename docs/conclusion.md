[Go to index](../README.md)

# Conclusion

## Goodbye and Next Steps
Hay nuevas ideas que el autor del curso incluyó para implementarlas por nuestra cuenta y seguir aprendiendo. De la misma manera nos mostró una serie de secciones que no vimos en el curso pero que de igual manera son muy importantes, como lo son las secciones de **Queues**, **Events**, **Compiling Assets**, **Test the code**.