[Go to index](../README.md)

# Filtering

## Avanced Eloquents Query Constraints
En lecciones pasadas vimos como filtrar, acá vimos como podmos filtrar por categoria agregando un nuevo query al método de **scopeFilter**. De esta forma podemos deshacernos del route de categories en el archivo **web.php** y simplemente agregariamos la ruta en nuestro archivo **_posts-header.blade.php**

## Extract a Category Dropdown Blade Component
Para dejar la repetición de código a la hora de pasar las categorias en muchas partes de nuestro proyecto, convertimos nuestro dropdown de categorias en un componente Blade. Podemos crear el componente desde la consola de la siguiente manera **php artisan make:component CategoryDropdown**, esto nos crea el componente y tambien una vista en la ruta `app/View` y en ese archivo colocamos las propiedades de las categorias para pasarlas a las demás vistas
```php
public function render()
    {
        return view('components.category-dropdown', [
            'categories' => Category::all(),
            'currentCategory' => Category::firstWhere('slug', request('category'))
        ]);
    }
```

## Author Filtering
Podemos hacer el filtrado de los autores de una forma parecida a la que hicimos el filtrado de categorias, creando un nuevo query en el método de **scopeFilter**, con eso filtramos los autores y de esta forma ya no es necesario el route de autores en el archivo **web.php**, por lo cual se puede eliminar, quedando mucho más limpio el código.

## Merge Category and Search Queries
Para buscar simultaneamente un post que perteneza a una categoria y a su vez haya sido escrito por un único autor, debemos concaternarle al URI lo que queremos decirle que busque, en vez de eliminar cada vez que hacemos una busqueda por uno de estos.

## Fix a Confusing Eloquent Query Blog
Nos encontramos con un pequeño pero confuso bug a la hora de hacer el query a la base de datos por lo que la vista nos estaba devolviendo algo inesperado, pero se soluciono sencillamente, en la función de **scopeFilter**.
```php
public function scopeFilter($query, array $filters){
        
        $query->when($filters['search'] ?? false, fn($query, $search) =>
            $query->where(fn($query) =>
                $query->where('title', 'like', '%' . $search . '%')
                    ->orWhere('body', 'like', '%' . $search . '%')));    

        $query->when($filters['category'] ?? false, fn($query, $category) =>
            $query->whereHas('category', fn($query) => $query->where('slug', $category)));  
            
        $query->when($filters['author'] ?? false, fn($query, $author) =>
            $query->whereHas('author', fn($query) => $query->where('username', $author)));  
    }
```
