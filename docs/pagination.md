[Go to index](../README.md)

# Pagination

## Laughably Simple Pagination
Le podemos añadir paginación a nuestra aplicación por medio del método **paginate()**, para lograr esto lo podemos hacer desde nuestro **PostController**
```php
public function index(){
        return view('posts.index', [
            'posts' => Post::latest()->filter(request(['search', 'category','author'])
            )->paginate(6)->withQueryString()
        ]); 
    }
```
Esto nos crea un índice de páginas en las cuales podemos navegar. Luego en nustra vista principal tenemos que añadir lo siguiente, **{{$posts->links()}}** y esto nos permite crear una paginación sencilla y funcional. 