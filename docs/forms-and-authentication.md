[Go to index](../README.md)

# Forms and Authentication

## Build a Register User Page
Construimos una página de registro para nuestros usuarios. Primero tenemos que crear un route para crear usuarios y crear el controlador para los mismos, el cual lo podemos hacer utilizando artisan, con el siguiente comando **php artisan make:controller RegisterController**

Route
```php
Route::get('register', [RegisterController::class, 'create']);
```
En este controlador creamos un par de funciones, una que nos devuelve una vista y la otra para almacenar los usuarios
Route
```php
public function create(){
    return view('register.create');
}

public function store(){
    $attributes = request()->validate([
        'name' => ['required', 'max:255'],
        'username' => ['required', 'max:255', 'min:3'],
        'email' => ['required', 'email', 'max:255'],
        'password' => ['required', 'min:7', 'max:255']
    ]);

    User::create($attributes);

    return redirect('/');
}
```

## Automatic Password Hashing With Mutators
Una manera automatica de encriptar las contraseñas de los usuarios es creando una función en el modelo de usuarios que se ejecuta al setear un atributo en específico, el nombre de la función debe ser como el siguiente
```php
public function setPasswordAttribute($password){
    $this->attributes['password'] = bcrypt($password);
}
```

## Failed Validation and Old Input Data
Podemos hacer validaciones a los input de nuestro formulario, el cual validará las reglas de nuestros inputs por ejemplo, la mínima cantidad de caracteres, solo tenemos que utilizar el siguiente código
```php
@error('email')
    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
@enderror
```
También tenemos una opción para recobrar los datos que teniamos anteriormente en caso de que se muestre una validación de error
```php
    value="{{ old('email') }}"
```
Otro tipo de validación que tenemos es que los usuarios y los emails de nuestra base de datos sean únicos y que en caso de que una persona digite un usuario o email ya existente, le muestre un mensaje de error, para lograr esto tenemos que modificar nuestra función de store en el controlador de registros
```php
    public function store(){
        $attributes = request()->validate([
            'name' => ['required', 'max:255'],
            'username' => ['required', 'min:3', 'max:255', Rule::unique('users', 'username')],
            'email' => ['required', 'email', 'max:255', Rule::unique('users', 'email')],
            'password' => ['required', 'min:7', 'max:255']
        ]);

        User::create($attributes);

        return redirect('/');
    }
```

## Show a Success Flash Message
Podemos mostrar un mensaje en caso de que un usuario se cree exitosamente, para así dar más información al usuario, para crear el mensaje simplemente necesitamos crear el mensaje de error y un pequeño componente
```php
    return redirect('/')->with('success', 'Your account has been created');
```
De esta forma redireccionamos al usuario a la página principal pero con el mensaje de éxito

Componente
```php
@if (session()->has('success'))
    <div x-data="{show:true}"
        x-init="setTimeout(() => show = false, 4000)"
        x-show="show"
        class="fixed bg-blue-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm"
    >
        <p>{{ session('success') }}</p>
    </div>
@endif
```

## Login and Logout
Para hacer el login de un usuario podemos utilizar `auth()->login($user)`. Utilizamos middlewares para mostrar o esconder elementos dependiendo de si el usuario esta logeado o no. Luego creamos el route de logout.
```php
Route::post('logout', [SessionsController::class, 'destroy']);
```
También creamos su respectivo controlador desde artisan con el comando **php artisan make:controller SessionsController** en el cual creamos un método para destruir la sesión o hacer logout
```php
public function destroy(){
    auth()->logout();

    return redirect('/')->with('success', 'Goodbye!');
    }
```

## Build the Log In Page
Para poder el login con los usuarios que ya estan registrados primero necesitamos crear el route con su middleware
```php
Route::post('login', [SessionsController::class, 'store'])->middleware('guest');
```
También tenemos que crear la vista para el formulario del LogIn, una vez creada la vista creamos un método para que nos redireccione a esta misma 
```php
public function create(){
        return view('sessions.create');
    }
```
Por último creamos un método en el controlador de sesiones, el cual se encarga de validar los datos enviados en el form y ver si son correctos, y dependiendo de si la información es correcta o no, dar una respuesta.
```php
public function store(){
        $attributes = request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (auth()->attempt($attributes)){
            session()->regenerate();

            return redirect('/')->with('success', 'Welcome Back!');
        }

        throw ValidationException::withMessages([
            'email' => 'Your provided credentials could not be verified'
        ]);
    }
```

## Laravel Breeze Quick Peek
Hay una manera rapida y sencilla de crear todo lo que hemos creado hasta este punto, esta manera es utilizar una funcionalidad implementada en laravel llamada **Laravel Breeze**, esta implementación ya trae todas las caracteristicas de autenticación de Laravel, incluyendo el login, registro, cambiar la contraseña, verificación de correos y confirmación de contraseñas. Para utilizar esta implementación lo podemos hacer creando un nuevo proyecto e instalando Laravel Breeze en este mismo proyecto, utilizando el comando **composer require laravel/breeze --dev**. Una vez instalado Laravel Breeze utilizamos el comando **php artisan breeze:install** y seguidamente el comando **npm install npm run dev**, para utilizar este último comando tenemos que tener instalado node.js. De esta manera deberiamos ser capaces de instalar nuestras dependencias y tener todo configurado correctamente.
