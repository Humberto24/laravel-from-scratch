[Go to index](../README.md)

# Blade

## Blade: The Absolute Basics
Blade es un motor de plantillas que se utilizan especificamente en las vistas, nos ofrece código más sencillo a la hora de programar, en lugar de escribir **<?php echo $post->title; ?>**, blade nos da la opción de escribir **{{$post->title}}**, de esta manera modificaremos nuestras vistas
Archivo posts.blade.php
```php
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    @foreach ($posts as $post)
    <article class="{{$loop->even ? 'foobar' : ''}}">
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>    
        </h1>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
</body>
</html>
```

Archivo post.blade.php
```php
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <article>
        <h1>{{$post->title}}</h1>

        <div>
            {!!$post->body!!}
        </div>
    </article>

    <a href="/">Go back</a>
</body>
</html>
```

## Blade: Layouts Two Ways
Blade nos permite utilizar dos tipos de plantillas muy sencillas y faciles de usar que nos ayudan a hacer nuestro código más rapidamente. Lo único que debemos hacer para utilizar estas plantillas es crear una vista en la carpeta `resources/views` a la cual llamaremos **layout.blade.php** y utilizaremos unicamente la primera forma ya que fue la de mi preferencia, la vista **layout.blade.php** debe quedar asi 

```php
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>

<body>
    @yield ("content")
</body>
</html>
```
Y debemos modificar el archivo **posts.blade.php** de la siguiente manera
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>
        </h1>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
```
Y el archivo **post.blade.php** de la siguiente manera
```php
@extends ("layout")

@section ("content")
    <article>
        <h1>{{$post->title}}</h1>

        <div>
            {!!$post->body!!}
        </div>
    </article>

    <a href="/">Go back</a>
@endsection
```

## A Few Tweaks and Consideration
En esta instancia del proyecto, por la forma en la que estamos cargando los posts podemos borrar el constraint que agregamos anteriormente en el archivo **web.php**, quedando de la siguiente manera 
```php
Route::get('/', function () {

    return view('posts', [
        'posts' => Post::all()
    ]); 
});

Route::get('posts/{post}', function ($slug) {
    
    return view('post', [
        'post' => Post::findOrFail($slug)
    ]);
});
```
Una vez que quitamos el constraint podemos crear una nueva función en el modelo **Post.php** en la carpeta `app/Models` quedando de la siguiente manera
```php
public static function findOrFail($slug)
    {
        $post = static::find($slug);
        
        if (! $post){
            throw new ModelNotFoundException();
        }

        return $post;
    }
```

