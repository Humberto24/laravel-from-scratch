[Go to index](../README.md)

# Admin Section

## Limit Access to Only Admins
Para dar acceso a una página solo a las personas que estan registradas como administradores, primero nos creamos el route con el endpoint al cual queremos ir y le agregamos el middleware.
```php
Route::get('admin/posts/create', [PostController::class, 'create'])->middleware('admin');
```
Podemos agregar pequeños bloques de seguridad para solamente admitir a los administradores
```php
 if(auth()->guest()){
    abort(Response::HTTP_FORBIDDEN);
}

if(auth()->user()->username !== 'admin'){
    abort(403);
}
```
Para activar nuestro middleware debemos ir a la ruta `app/Http` y en el archivo **Kermel.php** agregamos lo siguiente
```php
'admin' => \App\Http\Middleware\MustBeAdministrator::class
```

## Create the Publish Post Form
Creamos el formulario para publicar nuevos posts, creamos varios inputs para que el usuario digite los datos y un dropdown dinámico con las categorias que tenemos en la base de datos para que el usuario pueda asociar un post con una categoria. Implementamos varias restricciones, como que el slug del post fuera único. Para lograr esto simplemente creamos la vista, creamos un route y creamos nuestro método **store** en el controlador de los posts para que haga las validaciones y nos cree las publicaciones
```php
Route::post('admin/posts', [PostController::class, 'store'])->middleware('admin');
```
```php
public function store()
    {
        $attributes = request()->validate([
            'title' => 'required',
            'slug' => ['required', Rule::unique('posts', 'slug')],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);

        $attributes['user_id'] = auth()->id();

        Post::create($attributes);

        return redirect('/');
    }
```

## Validate and Store Post Thumbnails
Al formulario de agregar publicaciones le añadimos la opción de colocar una imagen, agregando un nuevo label de tipo **file**. Tenemos que cambiar la ruta por defecto que tiene el filesystem, para esto tenemos que irnos a la ruta `config/filesystems.php` y cambiar el driver por defecto que trae
```php
'default' => env('FILESYSTEM_DRIVER', 'public'),
```
De esta manera los archivos de tipo **file** laravel los va a colocar en la ruta `storage/app/public`, luego utilizamos el comando **php artisan storage:link** para que nos cree un **symlink** que significa symbolic link, y agregmos el atributo de thumbnail que viene del formulario a nuestro método store en el **PostController**
```php
$attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
```
De esta manera le añadimos una imagen a los posts y las podemos usar en las vistas dinámicamente

## Extract Form-Specific Blade Components
Extrajimos las partes del form como lo son los label, textarea y botones a componentes Blade para limpiar nuestro código, de esta manera se ve más entendible y si quisieramos modificar los label, por ejemplo, podríamos hacerlo de una manera más sencilla
```php
<x-form.input name="title" />
<x-form.input name="slug" />
<x-form.input name="thumbnail" type="file" />
<x-form.textarea name="excerpt" />
<x-form.textarea name="body" />
```
Así se ven mucho más estructurados y sencillamente les pasamos las propiedades que necesitamos para que funcione todo correctamente

## Extend the Admin Layout
Añadimos una sección de dropdown en el cual pusimos varios links, uno para hacer logout, otro para ir al formulario de crear un nuevo post, también implementamos una función dinámica para saber en que página nos encontramos actualmente y darle un poco de estilo al link que nos lleva dicha página
```php
:active="request()->is('admin/posts/create')"
```

## Create a Form to Edit and Delete Posts
Creamos una página para editar y eliminar los posts que han sido publicados. Utilizamos elementos de un framework para crear una tabla y la personlizamos a nuestro gusto. Hicimos la tabla dinámica para que mostrara los posts que tenemos en la base de datos. Emigramos algunos métodos de un controlador a otro para tener más consistencia en nuestro código y creamos en este mismo controlador los métodos de eliminar y editar.
```php
public function update(Post $post)
    {
        $attributes = request()->validate([
            'title' => 'required',
            'thumbnail' => 'image',
            'slug' => ['required', Rule::unique('posts', 'slug')->ignore($post->id)],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);

        if(isset($attributes['thumbnail'])){
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $post->update($attributes);

        return back()->with('success', 'Post Updated!');
    }
```
```php
    public function destroy(Post $post)
    {
        $post->delete();

        return back()->with('success', 'Post Deleted!');
    }
```

## Group and Store Validation Logic
En nuestro AdminPostController tenemos un poco de código muy parecido, el cual podemos normalizar, extraer a una función y utilizar sin necesidad de ser repetitivos en nuestra programación. 
```php
protected function validatePost(?Post $post = null): array
    {
        $post ??= new Post();

        return request()->validate([
            'title' => 'required',
            'thumbnail' => $post->exists ? ['image'] : ['required', 'image'],
            'slug' => ['required', Rule::unique('posts', 'slug')->ignore($post)],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);
    }
```
Y par utilizar estos atributos, simplemente llamamos a la función
```php
$attributes = $this->validatePost($post);
```

## All About Authorization
Implementamos la funcionalidad de que únicamente los usuarios que son administradores puedan ver el dropdown que nos lleva a la página del dashbord y crear los posts, utilizamos la implementación que posee laravel llamada **Gate**, la cual es como declarar una puerta de autorización que permite solo a algunos usuarios entrar y a otros los rechaza
```php
Gate::define('admin', function (User $user){
            return $user->username == 'admin';
        });
```
Debido a que definimos un método **Gate** podemos eliminar el middleware **admin** y utilizar el middleware **can** que trae laravel por defecto y es un middleware de autorización especificamente, esto nos permite convertir nuestro route a una forma más legible y a la misma vez nos permite evitar la repetición de código
```php
Route::middleware('can:admin')->group(function (){
    Route::post('admin/posts', [AdminPostController::class, 'store']);
    Route::get('admin/posts/create', [AdminPostController::class, 'create']);
    Route::get('admin/posts', [AdminPostController::class, 'index']);
    Route::get('admin/posts/{post}/edit', [AdminPostController::class, 'edit']);
    Route::patch('admin/posts/{post}', [AdminPostController::class, 'update']);
    Route::delete('admin/posts/{post}', [AdminPostController::class, 'destroy']);
});
```
