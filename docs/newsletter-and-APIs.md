[Go to index](../README.md)

# Newsletter and APIs

## Mailchimp API Tinkering
Hay un servicio llamado Mailchimp, vamos a utilizar el API de este servicio para almacenar contactos. Primero debemos crearnos una cuenta gratuita en Mailchimp y una vez dentro debemos crearnos un API key, esta la podemos añadir a las variables del ambiente y luego configuramos el archivo `config/services.php`
```php
'mailchimp' => [
    'key' => env('MAILCHIMP_KEY')
]
```  
La documentación de Mailchimp nos da un route en cual podemos probar si todo funciona correctamente
```php
$mailchimp = new \MailchimpMarketing\ApiClient();

$mailchimp->setConfig([
	'apiKey' => 'YOUR_API_KEY',
	'server' => 'YOUR_SERVER_PREFIX'
]);

$response = $mailchimp->ping->get();

print_r($response);
```  
Acá colocamos los valores correspondientes y si todo funcionó correctamente el endpoint debería devolvernos esta respuesta
```php
{
   "health_status": "Everything's Chimpy!"
}
```
Luego podemos añadir a un nuevo usuario a nuestra lista con tan solo cambiar los metodos que nos ofrece este servicio.

## Make the Newsletter Form Work
Para relizar la funcionalidad del endpoint de newsletter y poder agregar usuarios dinámicamente y no quemando los datos como lo hicimos anteriormente simplemente debemos cambiar nuestro route para que valide los datos, nos redireccione y nos brinde información sobre la suscripción
```php
Route::post('newsletter', function (){
    request()->validate(['email' => 'required|email']);

    $mailchimp = new \MailchimpMarketing\ApiClient();

    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us18'
    ]);

    try {
        $response = $mailchimp->lists->addListMember('948b40325f', [
            'email_address' => request('email'),
            'status' => 'subscribed'
        ]);
    } catch (\Exception $e) {
        throw \Illuminate\Validation\ValidationException::withMessages([
            'email' => 'This email could not be added to our newsletter list.'
        ]);
    }

    return redirect('/')->with('success', 'You are now signed up for our newsletter!');
});
``` 

## Extract a Newsletter Service
Para limpiar un poco nuestro route de newsletter podemos crer un servicio para separar un poco el código y mantener el patrón de los routes, para esto creamos una carpeta en en la ruta de `apps` y dentro de esta carpeta nos creamos una clase de Newsletter en la cual inyectaremos nuestro código en diferentes métodos
```php
public function subscribe(string $email, string $list = null){

    $list ??= config('services.mailchimp.lists.subscribers');

    return $this->client()->lists->addListMember($list, [
        'email_address' => $email,
        'status' => 'subscribed'
    ]);
}

protected function client(){
    $mailchimp = new ApiClient();

    return $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us18'
    ]);
}
```
De la misma manera nos creamos un controlador para nustro Newsletter con un método llamado `__invoke`, este nombre hará que la función se dispare automaticamente cuando llamamos al controlador desde el route
```php
public function __invoke(Newsletter $newsletter){
        request()->validate(['email' => 'required|email']);

        try {
            $newsletter->subscribe(request('email'));
        } catch (Exception $e) {
            throw ValidationException::withMessages([
                'email' => $e
            ]);
        }

        return redirect('/')->with('success', 'You are now signed up for our newsletter!');
    }
```
Y así limpiamos nuestro route y mantenemos el orden y el patrón que llevabamos hasta el momento 
```php
Route::post('newsletter', NewsletterController::class);
```

## Toy Chests and Contracts
Acá vimos un poco sobre como funciona laravel internamente. Intentamos nuevos modos de tener todo funcionando correctamente, uno de estos modos fue utilizando el constructor para inicializr el cliente. Trabajamos en el archivo **AppServiceProviders** en el método `register` para agregar nuestros newsletter al proveedor, de esta manera laravel lo encuentra para instanciarlo. En la ruta `app/services/Newsletter` debería verse así
```php
public function __construct(protected ApiClient $client)
{
        
}

public function subscribe(string $email, string $list = null){

    $list ??= config('services.mailchimp.lists.subscribers');

    return $this->client()->lists->addListMember($list, [
        'email_address' => $email,
        'status' => 'subscribed'
    ]);
} 
```  
Nuestro método de register en el archivo de **AppServiceProvider.php***
```php
public function register()
    {
        app()->bind(Newsletter::class, function(){
            $client = new ApiClient();

            $client->setConfig([
                'apiKey' => config('services.mailchimp.key'),
                'server' => 'us18'
            ]);

            return new Newsletter($client);
        });
    }
```
Vimos como crear un contrato creando un archivo en la carpeta de `app/services`, el archivo tiene que ser una interfaz para poder crear el contrato, quedando de la siguiente manera
```php
interface Newsletter
{
    public function subscribe(string $email, string $list = null);
}
```
Para utilizar el contrato en las clases que queramos simplemente tenemos que usar el metodo implements
```php
class ConvertKitNewsletter implements Newsletter
```