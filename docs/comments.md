[Go to index](../README.md)

# Comments

## Write the Markup for a Post Comment
Podemos incluir comentarios en los posts, para que de esta forma cada post tenga un comentario estático por el momento y pueda verse como una aplicación más real. Para crear estos comentarios tenemos que añadirlos a la vista en la página **show.blade.php** o podemos crear un componente Blade para limpiar un poco nuestro código. Para la foto del usuario que escribió el comentario podemos usar una imagen aleatoria escribiendo lo siguiente, `<img src="https://i.pravatar.cc/60">`.

## Table Consistency and Foreign Key Constraints
Podemos añadir consistencia a la base de datos por medio de restricciones de foreign key. Creamos un modelo, controlador y algunos otros elementos para los cometarios que vamos a añadir a nuestra página por medio del comando **php artisan make:model Comment -mfc**, el cual nos crea todos los elementos requeridos. Podemos agregar el constraint de esta forma
```php
$table->foreignId('post_id')->constrained()->cascadeOnDelete();
```

## Make the Comments Section Dynamic
Para mostrar los comentarios de un post escritos por un usuario dinamicamente tenemos que crear los atibutos del comentario en **CommentFactory**
```php
public function definition()
    {
        return [
            'post_id' => Post::factory(),
            'user_id' => User::factory(),
            'body' => $this->faker->paragraph()
        ];
    }
```
Luego debemos crear sus respectivas relaciones como hemos aprendido en lecciones pasadas. Y por último agregamos los comentarios a la vista, iterando los comentarios que tenemos en la base de datos pertenecientes a un post en específico y pasandolos como propiedades a la vista requerida para poder mostrarlos correctamente
```php
@foreach ($post->comments as $comment)
    <x-post-comment :comment="$comment"/>
@endforeach
```

## Design the Comment Form
Creamos un formulario para añadir comentarios y le dimos estilo para hacerlo agradable a la vista, también vimos que si el código se vuelve repetitivo, no importa si es estilo, podemos sacar esto a un componente Blade para que esté disponible tan solo llamando al componente y de esta manera no tener que reescribir lo mismo una y otra vez, además esto limpia un poco nuestro código.

## Activate the Comment Form
Implementamos la funcionalidad del form que habiamos creado anteriormente para publicar un comentario en algún post, para lograr esto lo primero que hicimos fue crear un nuevo route para esta funcionalidad y su respectivo controlador
```php
Route::post('posts/{post:slug}/comments', [PostCommentsController::class, 'store']);
```
En el controlador creamos un método de validación y creación de los comentarios de la siguiente manera
```php
public function store(Post $post){

        request()->validate([
            'body' => 'required'
        ]);

        $post->comments()->create([
            'user_id' => request()->user()->id,
            'body' => request('body')
        ]);
        
        return back();
    }
``` 

## Some Light Chapter Clean Up
Podemos agregar un mensaje de error para informar al usuario de lo que esta pasando, de la misma manera podemos agregar un poco de validación por parte del navegador, pero cabe resaltar que nunca debemos confiarnos con las validaciones del navegador, no esta de más añadir validaciones de parte del servidor como ya lo hemos hecho, además podemos extraer pedazos de código repetitivos y que nos van a ser útiles más adelante a un conponente.
