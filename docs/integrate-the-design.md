[Go to index](../README.md)

# Integrate the Design

## Convert the HTML and CCS to Blade
Entrando a lo que seria diseño, podemos hacer todo el **HTML** de una forma más dinámica gracias a la ayuda de Blade. Utilizamos un template ya creado y lo adaptamos a nuestro proyecto utilizando Blade para darle una interfaz bastante agradable.

## Blade Components and CSS Grids
Manipulando el CSS de los template por medio del atributo **class** podemos lograr acomodar los elementos de las páginas de la forma que a nosotros nos guste. Utilizando componentes y Blade (el cual aprendimos en lecciones pasadas) se vuelve mucho más limpio el código y ademas estos mismos componentes son reutilizables por lo que es una muy buena practica y además nos ahorra tiempo, todo lo que tenemos que hacer para pasar propiedades de una página a otra es usar `@props(['posts'])` y de esta forma ya se compratirian las propiedades y podriamos accederlas en cualquier página. Utilizando **CSS Grids** podemos darle un muy buen estilo a nuestros templates.

## Convert the Blog Post Page
La página de **Post** la hicimos dinámica igual que con la página principal utilizando Blade y nuevos componentes para evitar la repetición de código y aprovechar la reutilización, creamos un componente de categorias para renderizar las categorias de los post en las diferentes páginas pero sin necesidad de la repetición de código.

## A Small JavaScript Dropdown Detour
Creamos un dropdown que permite mostrar las categorias de una forma dinámica utilizando javaScript. Utilizmos un cdn y lo colovamos en el template del layout. Creamos pequeñas funciones como mostrar la categoria que estabamos visualizando en el botón, ponerle un poco de color al background cuando el elemento tenia el focus. Cambiamos el route de las categorias para mostrar la categorias de una manera correcta
```php
Route::get('categories/{category:slug}', function (Category $category) {
    return view('posts', [
        'posts' => $category->posts,
        'currentCategory' => $category,
        'categories' => Category::all()
    ]);
});
```

## How to Extract a Dropdown Blade Component
La extraccióm del dropdown que creamos anteriormente con javaScript hacia un componente nos ayudó a simplificar el código y a verse un poco más limpio y agradable a la vista, creamos un nuevo componente y extrajimos algunos estilos y los pasamos como propiedades nuevamente. Le asignamos un nombre al route de categorias el cual va a ser como un identificador y el cual nos va a ayudar a identificar la categoria en la que estamos 
```php
Route::get('categories/{category:slug}', function (Category $category) {
    
    return view('posts', [
        'posts' => $category->posts,
        'currentCategory' => $category,
        'categories' => Category::all()
    ]);
})->name('category');
```

## Quick Tweaks and Clean-Up
Hicimos cambios minimos a las vistas que las hacen lucir más reales y llamativas, por ejemplo, añadimos espaciado entre textos y el dropdown de las categorias más real.

