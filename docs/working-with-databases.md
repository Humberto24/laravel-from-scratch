[Go to index](../README.md)

# Working with databases

## Environment Files and Database Connections
Aca creamos la base de datos y migramos las tablas, también vimos algunas variables del ambiente y modificamos el archivo .env con los datos correctos de nuestra base de datos para poder usarla en nuestro proyecto 

## Migrations: The Absolute Basics
Conocimos algunos comandos que vienen en conjunto con **php artisan migrate** como el rollback y el fresh. Al igual que vimos y  la siguiente función en en archivo **create_users_table.php** en la ruta `database/migrations` 
```php
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }
```

## Eloquent and the Active Record Pattern
Nos referimos a las clases como modelos elocuentes, que es la forma de laravel para interactuar con las tablas de nuestra base de datos. Utilizamos el comando **php artisan tinker** para crear un shell en el cual podemos utilizar los metodos **all()** y **find()** creados en la carpeta `app/Models/Post`, crear usuarios a traves de los comandos 
```php
$user = new User;
$user->name = "";
$user->email = "";
$user->password = "";
$user->save();
```

## Make a Post Model and Migration
Estaremos trabajando con eloquent y creamos modelos de este tipo por lo tanto procederemos a borrar el archivo **Post.php** en la ruta `app/Models`, luego utilizamos el comando **php artisan make:migration create_posts_table** para crear la migració, una vez hecho esto podemos ver nuestra nueva tabla en la ruta `database/migrations`. También podemos eliminar la carpeta posts que contiene nuestros posts que se encuentra en la ruta `resources/`, luego modificamos el archivo con la terminación **create_posts_table.php** según el ejemplo que estamos siguiendo, el cual creamos con la migración en la ruta `database/migrations` para que quede de la siguiente manera 
```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->text("excerpt");
            $table->text("body");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
```
Una vez hecho esto podemos volver a la consola y escribir el comando **php artisan make:model Post** para volver a crear nuestro modelo. Luego podemos crear un nuevo post corriendo el comando **php artisan tinker** para entrar al shell y digitar lo siguiente 
```php
$post = new App\Models\Post;
$post->title = "Eloquent is amazing";
$post->excerpt = "Lorem ipsum dolar sit amet.";
$post->body = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempora dolore perspiciatis tempore asperiores consequuntur fuga voluptate, consequatur ratione ipsum ad deleniti odio, adipisci veniam eos odit? Provident nemo magnam animi!";
$post->save();
```
De esta forma creariamos un nuevo post. Por ultimo modificamos el archivo **web.php** para cambiar los routes
```php
Route::get('/', function () {

    return view('posts', [
        'posts' => Post::all()
    ]); 
});

Route::get('posts/{post}', function ($id) {
    
    return view('post', [
        'post' => Post::findOrFail($id)
    ]);
});
```
Y de la misma manera modificamos el archivo **posts.blade.php**
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->id}}">
                {{$post->title}}
            </a>
        </h1>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
``` 

## Eloquent Updates and HTML Escaping
Podemos correr el comando **php artisan tinker** para abrir el shell, una vez adentro utilizamos el comando **$post = App\Models\Post::first();** para utilizar el primer elemento en la base de datos de nuestros posts. Una vez hecho esto nosostros tenemos la opción de utilizar html en el body por ejemplo, en lugar de solamente tenerlo en texto plano, para eso utilizamos el siguiente comando que le dara un poco de espaciado al cuerpo del post **$post->body = '<p>' . $post->body . '</p>';**.


## 3 Ways to Mitigate Mass Assignment Vulnerabilities
Hay 3 formas para mitigar la asignación masiva de algunos datos. Para ello tenemos que ir al archivo **Post.php** y agregar alguna de las 3 formas. 

```php
protected $fillable = ['title', 'excerpt', 'body'];
```
Esta forma basicamente nos dice los atributos que se nos permiten asignar masivamente, por lo tanto si enviamos una gran lista de datos, estos serian los unicos que importarian, ignorando todos los demas.

```php
protected $guarded = ['id'];
```
Esta forma nos dice que podemos asignar de forma masiva las propiedades que nosotros queramos **excepto** las que nosotros declaramos en la variable **$guarded**

```php
protected $guarded = [];
```
Esta última forma nos permite decir que nunca permitiremos la asignación masiva de datos.


## Route Model Binding
Podemos modificar los routes de la carpeta **web.php** para vincularlo con un modelo e identificarlo a traves de alguna propiedad, en nuestro caso, seria el slug. Quedaría de la siguiente manera
```php
Route::get('posts/{post:slug}', function (Post $post) {
    
    return view('post', [
        'post' => $post
    ]);
});
```

Para que esto funcione también debemos modificar el método **up** del archivo con terminación en **create_posts_table.php** en la ruta `database/migrations`, de la siguiente manera
```php
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string("slug")->unique();
            $table->string("title");
            $table->text("excerpt");
            $table->text("body");
            $table->timestamps();
        });
    }
```

## Your First Eloquent Relationship 
Podemos hacer relaciones entre una categoria y un post utilizando relaciones elocuentes, para esto primero debemos utilizar el comando **php artisan make:model Category -m**, de esta forma creamos un modelo y una nueva migración, las cuales podemos ver en las rutas `app/Models` y `database/migrations` respectivamente.
Modificamos el método **up** en el archivo creado de la carpeta `database/migrations`
```php
public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("slug");
            $table->timestamps();
        });
    }
```

Luego editamos en el archivo con terminación **create_posts_table.php** en la ruta `database/migrations` 
```php
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId("category_id");
            $table->string("slug")->unique();
            $table->string("title");
            $table->text("excerpt");
            $table->text("body");
            $table->timestamps();
        });
    }
```
Luego podemos correr el comando **php artisan migrate:fresh** para eliminar y volver a crear nuestras tablas y le añadimos algunos datos. Para ver la relación simplemente agregamos un **category_id** que concuerde con algún id de la tabla de posts. Creamos una función en el modelo **Post.php**
```php
public function category()
    {
        return $this->belongsTo(Category::class);
    }
```

Por último agregamos las categorias a las vistas, en el archivo **posts.blade.php**
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>
        </h1>

        <p>
            <a href="#">{{$post->category->name}}</a>
        </p>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
```

## Show All Posts Associated With a Category
Para mostrar todos los posts asociados a una categoria primero debemos crear un nuevo route el cual nos va a dirigir a la ruta que queremos ir dependiendo del slug, debe quedar de la siguiente manera
```php
Route::get('categories/{category:slug}', function (Category $category) {
    return view('post', [
        'post' => $category->posts
    ]);
});
```
Luego debemos modificar los modelos.

Modelo **Post.php**
```php
public function category()
    {
        return $this->belongsTo(Category::class);
    }
```

Modelo **Category.php**
```php
public function posts()
    {
        return $this->hasMany(Post::class);
    }
```

Luego debemos editar nuestras vistas en la carpeta `resources/views` para que puedan renderizar de la manera correcta
Vista **posts.blade.php**
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>
        </h1>

        <p>
            <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
        </p>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
```

Vista **post.blade.php**
```php
@extends ("layout")

@section ("content")
    <article>
        <h1>{{$post->title}}</h1>

        <p>
            <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
        </p>

        <div>
            {!!$post->body!!}
        </div>
    </article>

    <a href="/">Go back</a>
@endsection
```

## Clockwork, and the N+1 Problem
A lo largo del código entramos en un dilema o problema conocido como **n+1 problem** en el cual estamos accediendo a una relación dentro de una iteración, por lo cual estariamos ejecutando llamadas innecesarias a la base de datos y esto bajaria el rendimiento de la aplicación. Este problema lo podemos ver en la vista **posts.blade.php**
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>
        </h1>

        <p>
            <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
        </p>

        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
```
Una buena manera de resolver este problema es utilizando clockwork que nos da herramientas especificas de php. Esta herramienta la instalaremos a traves de **composer** utilizando el comando **composer require itsgoingd/clockwork**. Una vez instalada esta herramienta, necesitaremos agregar la extensión al navegador y con esto estariamos listos con las configuraciones. Para usar la herramienta debemos ir a developer tools en el navegador y ahí nos debería aparecer una opción llamada clockwork en el cual podemos revisar los querys que hemos ejecutado y algunas otras acciones 


## Database Seeding Saves Time
Database seed es una función que incluye php artisan que nos permite poblar nuestra base de datos con algunos datos ficticios, esto nos permite ahorrarnos tiempo repoblando la base de datos. 
Vimos como pudimos relacionar las tablas para mostrar la información del usuario en los posts, para lograr esto debemos modificar la función **up** el archivo con terminación **create_posts_table.php** 

```php
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id");
            $table->foreignId("category_id");
            $table->string("slug")->unique();
            $table->string("title");
            $table->text("excerpt");
            $table->text("body");
            $table->timestamps();
        });
    }
```
El archivo **DatabaseSeeder.php** en la ruta `database/migrations/seeders` lo debemos modificar de la siguinte manera, así estariamos creando estos datos cuando corramos el comando seed 
```php
public function run()
    {
        User::truncate();
        Post::truncate();
        Category::truncate();

        $user = User::factory()->create();

        $personal = Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        $family = Category::create([
            'name' => 'Family',
            'slug' => 'family'
        ]);

        $work = Category::create([
            'name' => 'Work',
            'slug' => 'work'
        ]);

        
        Post::create([
            'user_id' => $user->id,
            'category_id' => $family->id,
            'title' => 'My Family Post',
            'slug' => 'my-firsts-post',
            'excerpt' => '<p>Lorem ipsum dolar asit amet.</p>',
            'body' => '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel eos ex animi veniam ratione quia, suscipit voluptatibus quasi quas reprehenderit sit laboriosam tenetur dolores modi expedita! Quia vero perferendis natus?<p>'
        ]);

        Post::create([
            'user_id' => $user->id,
            'category_id' => $work->id,
            'title' => 'My Work Post',
            'slug' => 'my-work-post',
            'excerpt' => '<p>Lorem ipsum dolar asit amet.</p>',
            'body' => '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel eos ex animi veniam ratione quia, suscipit voluptatibus quasi quas reprehenderit sit laboriosam tenetur dolores modi expedita! Quia vero perferendis natus?</p>'
        ]);
    }
```
En el archivo con terminación en **create_categories_table.php** en la ruta `database/migrations` debemos agregar un par de **unique()** para que no se nos dupliquen a la hora de lanzar el seed
```php
public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string("name")->unique();
            $table->string("slug")->unique();
            $table->timestamps();
        });
    }
```

## Turbo Boost With Factories
Factories nos dan un gran poder y una mayor facilidad a la hora de ejecutar ciertas acciones en nustro código, **factory()** nos permite crear datos ficticios para nuestros modelos, para probar esta característica, haremos lo siguiente
Primero tenemos que crear el factory con el comando **php artisan make:factory**. Luego debemos modificar el factory que acabamos de crear 
```php
public function definition()
    {
        return [
            'user_id' => User::factory(),
            'category_id' => Category::factory(),
            'title' => $this->faker->sentence,
            'slug' => $this->faker-slug,
            'excerpt' => $this->faker->sentence,
            'body' => $this->faker->paragraph
        ];
    }
```
También modficamos el archivo **CategoryFactory.php** 
```php
public function definition()
    {
        return [
            'name' => $this->faker->word,
            'slug' => $this->faker->slug 
        ];
    }
```
Finalmente modificamos nuestro archivo **DatabaseSeeder.php**
```php
public function run()
    {
        Post::factory()->create();    
    }
```

## View All Posts By An Author
Podemos ver todos los posts asociados a un usuario, para lograr esto debemos modificar nuestra funcioón de user() en el archivo **Post.php**, quedando de la siguiente manera
```php
public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
```
Para identificar de una manera más estetica a nuestros usuarios podemos crearle un username, primero vamos a la migración **create_users_table.php**
```php
public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }
```
Luego debemos añadir ese username a su respectivo factory, en mi caso sería al archivo **UserFactory.php**
```php
public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'username' => $this->faker->unique()->userName(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
```
Cambiamos nuestros routes en el archivo **web.php**, debería verse de la siguiente manera
```php
Route::get('/', function () {

    return view('posts', [
        'posts' => Post::latest()-> with("category", "author")->get()
    ]); 
});

Route::get('posts/{post:slug}', function (Post $post) {
    
    return view('post', [
        'post' => $post
    ]);
});


Route::get('categories/{category:slug}', function (Category $category) {
    
    return view('post', [
        'post' => $category->posts
    ]);
});

Route::get('authors/{author:username}', function (User $author) {
    
    return view('post', [
        'post' => $author->posts
    ]);
});
```
Finalmente los mostramos en nuestras vistas
Archivo **posts.blade.php**
```php
@extends ("layout")

@section ("content")
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}">
                {{$post->title}}
            </a>
        </h1>

        <p>
            By <a href="/authors/{{ $post->author->username }}">{{$post->category->name}}</a> 
            in <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
        </p>


        <div>
            {{$post->excerpt}}
        </div>

    </article>
    @endforeach
@endsection
```
Archivo **post.blade.php**
```php
@extends ("layout")

@section ("content")
    <article>
        <h1>{{$post->title}}</h1>

        <p>
            By <a href="/authors/{{ $post->author->username }}">{{$post->category->name}}</a> 
            in <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
        </p>

        <div>
            {!!$post->body!!}
        </div>
    </article>

    <a href="/">Go back</a>
@endsection
```

## Eager Load Relationships on am Existing Model
Para hacer un **Eager Load**, que nos permite agregar los elementores que queramos como parte de nuestra llamada a la base de datos, primero debemos agregar un atributo y decirle que elementos queremos que cargue automaticamente a nuestro modelo de posts en el atributo **$with**, para hcaer esto debemos irnos al archivo **Post.php**, de la siguiente manera
```php
class Post extends Model
{
    use HasFactory;

    protected $guarded = []; 

    protected $with = ['category', 'authors'];
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
```
También debemos modificar nustros routes nuevamente
```php
Route::get('/', function () {

    return view('posts', [
        'posts' => Post::latest()->get()
    ]); 
});

Route::get('posts/{post:slug}', function (Post $post) {
    
    return view('post', [
        'post' => $post
    ]);
});


Route::get('categories/{category:slug}', function (Category $category) {
    
    return view('post', [
        'post' => $category->posts
    ]);
});

Route::get('authors/{author:username}', function (User $author) {
    
    return view('post', [
        'post' => $author->posts
    ]);
});
```