[Go to index](../README.md)

# Search

## Search (The Messy Way)
Vimos como buscar posts por medio de un filtro que digita el usuario utilizando el operdor **%** que nos permite buscar lo que esta antes, después o en medio del operador, por ejemplo, **%testing%** el cual buscaría un post que contenga este texto dentro de su titulo o cuerpo.

## Search (The Cleaner Way)
Acá limpiamos un poco el código que teniamos en los routes, creamos una categoria para emigrar el código de routes hacia esa categoria y simplemente llamabamos las funciones, conocimos lo que es un query builder el cual nos permite usar métodos como **when**.

Routes
```php
Route::get('/', [PostController::class, 'index'])->name('home');
Route::get('posts/{post:slug}', [PostController::class, 'show']);
```
PostController
```php
class PostController extends Controller
{
    public function index(){
        return view('posts', [
            'posts' => Post::latest()->filter(request(['search']))->get(),
            'categories' => Category::all()
        ]); 
    }

    public function show(Post $post){
        return view('post', [
            'post' => $post
        ]);
    }
}
```
