[Go to index](../README.md)

# The basics

## How a Route loads a View

Para iniciar y conocer un poco el ambiente nos vamos al archivo de rutas ubicado en `routes/web.php`
En `routes/web.php` es donde se declara cada ruta de su aplicación para que responda usualmente con una vista, también podria responder con una función, un html o un json 

Devolver una vista

```php
Route::get('/', function () {
    return view('welcome');
});
```

Devolver un html
 ```php
Route::get('/', function () {
    return "<h1>Hola Mundo</h1>";
});
```

Devolver un json
 ```php
Route::get('/', function () {
    return [foo => boo];
});
```

## Include CSS and JavaScript
Podemos editar el archivo `views/welcome.blade.php` y cambiarlo el default de la página para personalizarlo nosotros mismos

Podemos darle estilo a la página, para eso debemos ir a la carpeta public y crear un arhivo css por ejemplo `app.css` y le añadimos estilo de la siguiente manera

 ```css
body{
    background: navy;
    color: white;
}
```
Debemos recordar agregar la etiqueta link en el archivo `views/welcome.blade.php`.
De la misma manera podemos agregar funcionalidad con javascript, creando un archivo en la carpeta public y agregando el link en `views/welcome.blade.php`.

## Make a Route and Link to it
Podemos crear varias rutas con diferente end point, ya teniamos el que venia creado por defecto y podemos agregar nuevos como el siguiente 

 ```php
Route::get('/post', function () {
    return view('post');
});
```
Pero para que este end point funcione tenemos que crearle una vista en en la carpeta `views` con el nombre `post.blade.php` según este ejemplo

También podemos acceder a este end point por medio de links de la siguiente manera 

```html
<article>
        <h1><a href="/post">My First Post</a></h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga nemo accusamus eius quos aperiam  dicta, molestiae voluptates at laborum rem, maiores dolorem! Dolores harum eius animi rem? Libero, aperiam error?</p>
    </article>
```

## Store Blog Posts as HTML Files
Para hacer busquedas dinámicas a traves de html por medio del end point podemos cambiar algunos archivos, primero debemos modificar el archivo `post.blade.php` para agregarle una variable y de esta manera lograr que la página sea mas dinámica, debe quedar como el siguiente

```html
<!DOCTYPE html>
<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <article>
        <?= $post; ?>
    </article>

    <a href="/">Go back</a>
</body>
</html>
```
Luego creamos varias archivos en la carpeta `resources`, dentro de esta carpeta podemos crear otra carpeta y dentro de ella crear los archivos que necesitamos, para que cada uno de este archivo sea un nuevo post en lugar de tenerlos estáticos en un solo html. Cada archivo de quedar de la siguiente manera

```html
<!DOCTYPE html>
<head>
    <title>Document</title>
</head>
<body>
    <article>
        <h1><a href="/post">My First Post</a></h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga nemo accusamus eius quos aperiam dicta, molestiae voluptates at laborum rem, maiores dolorem! Dolores harum eius animi rem? Libero, aperiam error?</p>
    </article>
</body>
</html>
```
Por ultimo tenemos que modifcar el route en la ruta `routes/web.php` para que quede de la siguiente manera 

```php
Route::get('posts/{post}', function ($slug) {
    $path = __DIR__ . "/../resources/posts/{$slug}.html";

    if(!file_exists($path)){
        return redirect("/");
    }

    $post = file_get_contents($path);

    return view('post', [
        'post' => $post
     ]);
});
```
De esta forma el route recibe un segundo parametro que es lo que nos va a permitir buscar de una forma dinámica pasando la ruta del archivo al que queremos acceder.
Podemos renombrar a **welcome.blade.php** en la ruta `views/welcome.blade.php` como **posts.blade.php** para mayor facilidad. Entonces una vez renombrado, debemos ingresar al archivo **posts.blade.php** y que quede de la siguiente manera para poder ingresar a las rutas dinamicamente 

```html
<!DOCTYPE html>
<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <article>
        <h1><a href="/posts/my-first-post">My First Post</a></h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga nemo accusamus eius quos aperiam dicta, molestiae voluptates at laborum rem, maiores dolorem! Dolores harum eius animi rem? Libero, aperiam error?</p>
    </article>

    <article>
        <h1><a href="/posts/my-second-post">My Second Post</a></h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga nemo accusamus eius quos aperiam dicta, molestiae voluptates at laborum rem, maiores dolorem! Dolores harum eius animi rem? Libero, aperiam error?</p>
    </article>
    
    <article>
        <h1><a href="/posts/my-third-post">My Third Post</a></h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga nemo accusamus eius quos aperiam dicta, molestiae voluptates at laborum rem, maiores dolorem! Dolores harum eius animi rem? Libero, aperiam error?</p>
    </article>
</body>
</html>
```

## Route Wildcard Constraints
Podemos agregar algunas restricciones a nuestro segundo argumento para que sea solicitado de la forma que nosotros queramos, para lograr esto, tenemos que ir a `routes/web.php` y agregar la expresión **where** y la forma en que queremos que sea digitado el argumento, debe quedar de la siguiente manera   

```php
Route::get('posts/{post}', function ($slug) {
    $path = __DIR__ . "/../resources/posts/{$slug}.html";

    if(!file_exists($path)){
        return redirect("/");
    }

    $post = file_get_contents($path);

    return view('post', [
        'post' => $post
     ]);
})->where('post', '[A-z_\-]+');
```

## Use Caching for Expensive Operations
Podemos cachear la ruta de la página a la cual ingresamos para evitar almacenarla en memoria, para lograr esto modificamos el archivo `routes/web.php`, quedando de la siguiente manera

```php
Route::get('posts/{post}', function ($slug) {

    if(!file_exists($path = __DIR__ . "/../resources/posts/{$slug}.html")){
        return redirect("/");
    }

    $post = cache()->remember("posts.{$slug}", 5, fn() => file_get_contents($path));

    return view('post', ['post' => $post]);
})->where('post', '[A-z_\-]+');
```

## Use the File System Class to Read a Directory
Podemos usar archivos incorporados en el sistema para hacer más facil el dinamismo de nuestra aplicación, también podemos crear nuevos modelos y dentro de estos modelos crear funciones que nos sean utiles utilizando funciones globales ya incorporadas en el framework
Primero podemos modificar el archivo **web.php** para que quede de la siguiente manera

```php
<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('posts', [
        'posts' => Post::all()  
    ]);
    
});

Route::get('posts/{post}', function ($slug) {
    
    return view('post', [
        'post' => Post::find($slug)
    ]);

})->where('post', '[A-z_\-]+');
```
Recuerden que deben tener importado los modelos y librerias necesarias como se muestra en la imagen anterior. Una vez modificada este archivo debemos ir a crear el modelo con sus respectivas funciones, debemos irnos a la ruta `app/Models` y dentro de esta carpeta creamos una nueva clase y dentro de ella, debemos crear las funciones de esta manera

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModuleNotFoundException;
use Illuminate\Support\Facades\File;

class Post
{
    public static function all(){
        $files = File::files(resource_path("posts/"));

        return array_map(fn($file) => $file->getContents(), $files);
    }

    public static function find($slug){

        if(!file_exists($path = resource_path("posts/{$slug}.html"))){
            throw new ModelNotFoundException();
        }

        return cache()->remember("posts.{$slug}", 1200, fn() => file_get_contents($path));
    }
}
```
Luego debemos modificar el el archivo **posts.blade.php** en la ruta `resources/views`, para que quede asi

```html
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <?php foreach ($posts as $post) : ?>
    <article>
        <?= $post; ?>
    </article>
    <?php endforeach; ?>
</body>
</html>
```
De esta forma estariamos iterando los posts que tenemos y podriamos renderizarlos

## Find a Composer Package for Post Metadata
Primero debemos instalar YamlFrontMatter que es una libreria que nos ayudará a parsear metadata, intalaremos esta libreria por medio de composer con el comando **composer require spatie/yaml-front-matter** para más información puede acceder al siguiente link https://github.com/spatie/yaml-front-matter.
Luego inicializaremos algunos atributos en la clase Post que creamos anteriormente y también crearemos un constructor, esta carpeta esta localizada en `app/Model`

```php
    public $title;
    public $excerpt;
    public $date;
    public $body;
    public $slug;

    public function __construct($title, $excerpt, $date, $body, $slug){
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
    }
```
Luego debemos modificar el archivo **posts.blade.php** para mostrar los metadatos que queremos mostrar

```html
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <?php foreach ($posts as $post) : ?>
    <article>
        <h1>
            <a href="/posts/<?= $post->slug; ?>">
                <?= $post->title; ?>
            </a>    
        </h1>

        <div>
        <?= $post->excerpt; ?>
        </div>

    </article>
    <?php endforeach; ?>
</body>
</html>
```
Modificamos nuevamente el archivo **web.php** de la siguiente manera

```php
Route::get('/', function () {

    return view('posts', [
        'posts' => Post::all()
    ]); 
});

Route::get('posts/{post}', function ($slug) {
    
    return view('post', [
        'post' => Post::find($slug)
    ]);
})->where('post', '[A-z_\-]+');
```
Debemos modificar también las funciones **find** y **all** de la clase **Post** para que puedan coincidir con las rutas que estamos enviando en el archivo **web.php**
```php
public static function all()
    {
        return collect(File::files(resource_path("posts")))
            ->map(fn($file) => YamlFrontMatter::parseFile($file))
            ->map(fn($document) => new Post(        
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            ));
    }

    public static function find($slug)
    {
        return static::all()->firstWhere("slug", $slug);  
    }
```
De la misma manera debemos modificar el archivo **post.blade.php**

```html
<!DOCTYPE html>

<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>
    <article>
        <h1><?= $post->title; ?></h1>

        <div>
        <?= $post->body; ?>
        </div>
    </article>

    <a href="/">Go back</a>
</body>
</html>
```

## Collection Sorting and Caching Refresher
Para ordenar como queremos que se vea la vista podemos hacer un **sortBy** en el método **all**, y para cachear tenemos que utilizar **cache()-rememberForever**, todo esto en el archivo **Post.php**

```php
public static function all()
    {
        return cache()->rememberForever("posts.all", function(){
            return collect(File::files(resource_path("posts")))
            ->map(fn($file) => YamlFrontMatter::parseFile($file))
            ->map(fn($document) => new Post(        
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            ))->sortBy("date");
        });
    }
```