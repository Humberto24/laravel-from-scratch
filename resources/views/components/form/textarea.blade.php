@props(['name'])
<div class="mb-6">
    <x-form.label name="{{$name}}" />
    <textarea class="border border-gray-200 p-2 w-full rounded" id="{{$name}}" name="{{$name}}"
        required {{ $attributes }}>{{ $slot ?? old($name) }}</textarea>

    <x-form.error name="{{$name}}"/>
</div>