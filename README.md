# Laravel from the scratch 

## Index 

- [The basics](./docs/the-basics.md)
- [Blade](./docs/blade.md)
- [Working with databases](./docs/working-with-databases.md)
- [Integrate the Design](./docs/integrate-the-design.md)
- [Search](./docs/search.md)
- [Filtering](./docs/filtering.md)
- [Pagination](./docs/pagination.md)
- [Comments](./docs/comments.md)
- [Newsletter and APIs](./docs/newsletter-and-APIs.md)
- [Admin Section](./docs/admin-section.md)
- [Conclusion](./docs/conclusion.md)